﻿using UnityEngine;
using System.Collections;

public class Rotator : MonoBehaviour 
{
    void Start()
    {
        var random = new System.Random();
        var groundY = GameObject.FindGameObjectWithTag("Ground").transform.position.y;

        transform.position = new Vector3(random.Next(-9, 9), groundY + 1, random.Next(-9, 9));
    }
    
    void Update()
	{
	    transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
	}
}
