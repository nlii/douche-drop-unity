﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public float level = 1.0f;

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        var movement = new Vector3(moveHorizontal, 0, moveVertical);
        rigidbody.AddForce(movement * speed * level * Time.deltaTime);

        //var invMovementVector = new Vector3(movement.x * -1.0f, movement.y, movement.z * -1.0f);

        var playerPos = gameObject.transform.position;

        if (playerPos.x > 9.5f || playerPos.x < -9.5f || playerPos.z > 9.5f || playerPos.z < -9.5f)
        {
            var momentumDirection = rigidbody.velocity.normalized;
            momentumDirection.y = 0;
            rigidbody.velocity = new Vector3(0.0f, rigidbody.velocity.y, 0.0f);
            rigidbody.angularVelocity = new Vector3(0.0f, rigidbody.angularVelocity.y, 0.0f);
            rigidbody.AddForce(-momentumDirection * speed * 40 * Time.deltaTime);
        }

        //if (playerPos.x > 10)
        //{
        //    gameObject.transform.position = new Vector3(9, playerPos.y, playerPos.z);
        //}

        //if (playerPos.x < -10)
        //{
        //    gameObject.transform.position = new Vector3(-9, playerPos.y, playerPos.z);
        //}

        //if (gameObject.transform.position.z > 10)
        //{
        //    gameObject.transform.position = new Vector3(playerPos.x, playerPos.y, 9);
        //}

        //if (gameObject.transform.position.z < -10)
        //{
        //    gameObject.transform.position = new Vector3(playerPos.x, playerPos.y, -9);
        //}
    }

    void OnTriggerEnter(Collider other)
    {
        var collider = this.GetComponent<SphereCollider>();

        if (other.gameObject.tag == "PickUp")
        {
            ScoreController.IncreaseScore(100);
            other.gameObject.renderer.enabled = false;
            collider.isTrigger = true;
            StartCoroutine(WaitRemoveTrigger());
        }
    }

    IEnumerator WaitRemoveTrigger()
    {
        yield return new WaitForSeconds(2.0f);
        var collider = GetComponent<SphereCollider>();
        collider.isTrigger = false;
        MoveWorld();
    }

    void MoveWorld()
    {
        level += 0.1f;
        Debug.Log(level);

        var groundAndWalls = GameObject.FindGameObjectsWithTag("Ground");
        var playerY = this.gameObject.transform.position.y;

        foreach (var groundObj in groundAndWalls)
        {
            var groundPos = groundObj.transform.position;
            groundObj.transform.position = new Vector3(groundPos.x, playerY - 5, groundPos.z);
        }

        var cube = GameObject.FindGameObjectWithTag("PickUp");
        cube.renderer.enabled = true;
        GameObject.Instantiate(cube);
        cube.SetActive(false);
    }
}
